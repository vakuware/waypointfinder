This tool is meant to convert waypoint-files exported by ECDIS into usable instructions.
The output will be either a csv file or a table-like text file.

Features:
- waypoint ID
- name
- LAT coordinates
- LON coordinates
- remaining distance
- distance from current pos to waypoint
- course from current pos to waypoint
- geometry type
- speed

Requirenments:

JAVA 8 installation