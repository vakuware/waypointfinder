### Waypoints

This waypoint Plugin will focus loading and displaying waypoints.

Features include:
- A table-view of waypoints
- Import functionality from waypointfiles