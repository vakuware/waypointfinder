package de.vaku.waypoint.outtype;

import de.vaku.waypoint.Waypoint;
import de.vaku.waypoint.WaypointFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OutCSV extends OutType{
    public OutCSV() {
        super("csv", "The output will be a comma-separated file, ready to be opened by excel");
    }

    public List<String> createLinesCSV(WaypointFile wpFile) {
        List<String> lines = new ArrayList<>();
        boolean headerSet = false;
        for (Waypoint wp : wpFile.getWaypointList()) {
            if (!headerSet) {
                lines.add(wp.getFeatures().stream().map(Waypoint.Feature::getKey)
                        .collect(Collectors.joining(",")));
                headerSet = true;
            }
            lines.add(wp.toString());
        }
        return lines;
    }

    @Override
    public boolean performWriting(WaypointFile wpFile, String outFileName) {
        return writeLines(outFileName, createLinesCSV(wpFile));
    }
}
