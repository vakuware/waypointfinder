package de.vaku.waypoint.outtype;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.utils.Utils;
import de.vaku.waypoint.WaypointFile;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public abstract class OutType {
    private final String extension;
    private final String description;

    private final Collection<Setting<?>> settings;

    public OutType(String extension, String description) {
        this.extension = extension;
        this.description = description;
        this.settings = new ArrayList<>();
    }

    public String getExtension() {
        return extension;
    }

    public String getDescription() {
        return description;
    }

    public boolean writeToFile(WaypointFile wpFile, String outFileName) {
        boolean cancel = false;
        if (settings.size() > 0) {
            JPanel listPane = new JPanel();
            listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
            for (Setting<?> setting : settings) {
                listPane.add(setting.getComponent());
            }
            int res = JOptionPane.showConfirmDialog(null,
                    listPane,
                    "Print options :^)",
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE);
            if (res != 0) {
                cancel = true;
            }
        }
        if (!cancel) {
            return performWriting(wpFile, outFileName);
        } else {
            return false;
        }
    }

    public abstract boolean performWriting(WaypointFile wpFile, String outFileName);

    public static boolean writeLines(String outFileName, List<String> lines) {
        try {
            File outFile = new File(outFileName);
            Files.write(outFile.toPath(), lines);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void addSetting(Setting<?> setting) {
        settings.add(setting);
    }

    public static List<String> createHeader(WaypointFile wpFile, boolean addLine) {
        List<String> lines = new ArrayList<>();
        lines.add("Route: " + wpFile.getRouteName());
        lines.add("Date:  " + new SimpleDateFormat("HH:mm dd/MM/yyyy").format(new Date()) + " (converted)");
        if (addLine)
            lines.add(Utils.repeat("_", Main.MAX_CHARS));
        if (wpFile.getWaypointList().size() > 0) {
            lines.add("Total distance remaining: " + wpFile.getWaypointList().get(0).remain.getRepresentation() + " sm");
            if (addLine) {
                lines.add(Utils.repeat("_", Main.MAX_CHARS));
                lines.add("");
            }
        }

        return lines;
    }

    public static List<String> createFooter() {
        List<String> lines = new ArrayList<>();
        lines.add("Courses and Distances may differ from ECDIS calculations due to e. g. turning circle settings!");
        return lines;
    }

    public static abstract class Setting<T> {
        public Setting(OutType type) {
            type.addSetting(this);
        }

        public abstract Component getComponent();

        public abstract T getState();
    }

    public static class BooleanSetting extends Setting<Boolean> {
        private final JCheckBox checkBox;

        public BooleanSetting(OutType type, String name) {
            super(type);
            checkBox = new JCheckBox(name);
        }

        @Override
        public Component getComponent() {
            return checkBox;
        }

        @Override
        public Boolean getState() {
            return checkBox.isSelected();
        }
    }
}
