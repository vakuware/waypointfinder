package de.vaku.waypoint.outtype;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;

import de.vaku.waypoint.Waypoint;
import de.vaku.waypoint.WaypointFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class OutPDF extends OutType {

    public static PageSize PAGE_SIZE = PageSize.A4;
    private final BooleanSetting showBoxes = new BooleanSetting(this, "showBoxes");

    public OutPDF() {
        super("pdf", "A file with a table neatly formatted as pdf");
    }

    public void createTable(Document document, WaypointFile wpFile) {
        float[] w = new float[]{1};
        if (wpFile.getWaypointList().size() > 0) {
            Waypoint wp = wpFile.getWaypointList().get(0);
            List<Integer> widths = wp.getFeatures().stream().map(Waypoint.Feature::getMaxChars).collect(Collectors.toList());
            w = new float[widths.size()];
            for (int i = 0; i < widths.size(); i++) {
                w[i] = widths.get(i);
            }
        }
        Table table = new Table(w, true);
        document.add(table);
        boolean headerSet = false;
        List<Waypoint> waypointList = wpFile.getWaypointList();

        for (Waypoint wp : waypointList) {
            if (!headerSet) {
                wp.getFeatures().forEach(feature ->
                        table.addHeaderCell(getCell(feature.getKey(), showBoxes.getState(), false)));
                headerSet = true;
            }
            wp.getFeatures().forEach(feature ->
                    table.addCell(getCell(feature.getRepresentation(), showBoxes.getState(), feature.isAlignedRight())));
        }

        table.complete();
    }

    private Cell getCell(String text, boolean showBorder, boolean alignRight) {
        Cell cell = new Cell().add(new Paragraph(text));
        if (!showBorder)
            cell.setBorder(Border.NO_BORDER);
        if (alignRight)
            cell.setTextAlignment(TextAlignment.RIGHT);
        return cell;
    }

    public void createTitle(Document document, String str) {
        Table table = new Table(1);
        //table.setWidth(PAGE_SIZE.getWidth() - document.getLeftMargin() - document.getRightMargin());
        table.useAllAvailableWidth();
        Cell cell = new Cell()
                .add(new Paragraph(str))
                .setTextAlignment(TextAlignment.CENTER)
                .setFontSize(20)
                .setBorder(Border.NO_BORDER);
        table.addCell(cell);
        document.add(table);
    }

    @Override
    public boolean performWriting(WaypointFile wpFile, String outFileName) {
        try {
            String tempOutFileName = outFileName + ".temp";

            PdfWriter writer = new PdfWriter(tempOutFileName, new WriterProperties().setPdfVersion(PdfVersion.PDF_2_0));
            PdfDocument pdfDocument = new PdfDocument(writer);
            pdfDocument.setTagged();

            Document document = new Document(pdfDocument, PAGE_SIZE);
            List<String> header = createHeader(wpFile, false);
            String routeTitle = header.get(0);
            createTitle(document, routeTitle);
            header.remove(0);


            Paragraph descParagraph = new Paragraph();
            descParagraph.add(new Text("\n"));
            descParagraph.add(new Text(String.join("\n", header)));
            descParagraph.add(new Text("\n"));
            document.add(descParagraph);

            createTable(document, wpFile);

            document.add(new Paragraph(String.join("\n", createFooter())));


            document.close();

            PdfDocument pdfDoc = new PdfDocument(new PdfReader(tempOutFileName), new PdfWriter(outFileName));
            Document doc = new Document(pdfDoc);

            int numberOfPages = pdfDoc.getNumberOfPages();

            String date = new SimpleDateFormat("HH:mm dd/MM/yyyy").format(new Date());
            for (int i = 1; i <= pdfDoc.getNumberOfPages(); i++) {
                Rectangle pageSize = pdfDoc.getPage(i).getPageSize();
                doc.showTextAligned(new Paragraph(date).setItalic(),
                        pageSize.getLeft() + 40, pageSize.getBottom() + 20,
                        i, TextAlignment.LEFT, VerticalAlignment.BOTTOM, 0);
                doc.showTextAligned(new Paragraph(routeTitle).setItalic(),
                        pageSize.getWidth() / 2, pageSize.getBottom() + 20,
                        i, TextAlignment.CENTER, VerticalAlignment.BOTTOM, 0);
                doc.showTextAligned(new Paragraph(String.format("Page %s of %s", i, numberOfPages)).setItalic(),
                        pageSize.getWidth() - 40, pageSize.getBottom() + 20,
                        i, TextAlignment.RIGHT, VerticalAlignment.BOTTOM, 0);
            }
            doc.close();

            boolean deleted = new File(tempOutFileName).delete();
            if (!deleted) {
                wpFile.getMain().appendStatusText("Could not delete temporary file " + tempOutFileName);
            }
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return true;
    }
}
