package de.vaku.waypoint.outtype;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.utils.Utils;
import de.vaku.waypoint.Waypoint;
import de.vaku.waypoint.WaypointFile;

import java.util.List;

public class OutTXT extends OutType {
    public OutTXT() {
        super("txt", "The output will be a text file ready to be printed");
    }

    public List<String> createLinesTXT(WaypointFile wpFile) {
        List<String> lines = createHeader(wpFile, true);
        boolean headerSet = false;
        for (Waypoint wp : wpFile.getWaypointList()) {
            if (!headerSet) {
                lines.add(createTabHeader(wp));
                lines.add(Utils.repeat("_", Main.MAX_CHARS));
                headerSet = true;
            }
            lines.add(createTabString(wp));
        }
        lines.add("");
        lines.addAll(createFooter());
        return lines;
    }

    public static String createTabHeader(Waypoint waypoint) {
        StringBuilder builder = new StringBuilder();
        for (Waypoint.Feature<?> feature : waypoint.getFeatures()) {
            String key = feature.getKey();
            key += Utils.repeat(" ", feature.getMaxChars() - key.length());
            key = key.substring(0, feature.getMaxChars());
            builder.append(key);
        }
        return builder.toString();
    }

    public static String createTabString(Waypoint waypoint) {
        StringBuilder builder = new StringBuilder();
        for (Waypoint.Feature<?> feature : waypoint.getFeatures()) {
            String rep = feature.getRepresentation();
            rep += Utils.repeat(" ", feature.getMaxChars() - rep.length());
            builder.append(rep);
        }
        return builder.toString();
    }

    @Override
    public boolean performWriting(WaypointFile wpFile, String outFileName) {
        return writeLines(outFileName, createLinesTXT(wpFile));
    }
}
