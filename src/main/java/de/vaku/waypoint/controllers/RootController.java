package de.vaku.waypoint.controllers;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.plugins.Plugin;
import de.vaku.waypoint.utils.DraggingTabPaneSupport;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Views
 * registerView(name, defaultPosition)
 * setViewVisible(name, true/false)
 * setViewVisible(name, true/false, position)
 * isViewVisible(name)
 * getView(name)
 *
 * Menu
 * addFileMenu(Name, MenuItem)
 *
 */
public class RootController extends FXController<BorderPane> {

    private Map<Plugin, TabView> tabViews;
    private Map<Plugin, Stage> stages;

    public void init(Main main, Stage rootStage, BorderPane root) {
        super.init(main, rootStage, root);
        tabViews = new HashMap<>();
        stages = new HashMap<>();
        setupTabPanes();
    }

    @FXML
    private MenuBar mMain;

    @FXML
    private Menu mMainFile;

    @FXML
    private Menu mMainView;

    @FXML
    private Menu mMainHelp;

    @FXML
    private BorderPane rootPane;

    @FXML
    private SplitPane vertSplitPane;

    @FXML
    private SplitPane horSplitPane;

    @FXML
    private TabPane leftTabs;

    @FXML
    private TabPane centerTabs;

    @FXML
    private TabPane rightTabs;

    @FXML
    private TabPane bottomTabs;

    private TabPane[] tabPanes;


    public Stage registerStage(Plugin key, String displayName, Parent parent) {
        if (stages.containsKey(key)) {
            throw new IllegalStateException(Main.I18N.get("warning.stage.already_registered", key));
        } else {
            Stage stage = createStage(displayName, parent);
            stages.put(key, stage);
            return stage;
        }
    }

    public void setStageVisible(Plugin key, boolean visible) {
        Stage stage = getStage(key);
        if (stage.isShowing() && !visible) {
            stage.close();
        } else if (!stage.isShowing() && visible) {
            stage.showAndWait();
        }
    }

    private Stage createStage(String displayName, Parent parent) {
        Stage stage = new Stage();
        stage.setScene(new Scene(parent, 600, 400));
        stage.setTitle(displayName);
        stage.initOwner(getRootStage());
        stage.initModality(Modality.APPLICATION_MODAL);

        return stage;
    }

    public Stage getStage(Plugin key) {
        if (stages.containsKey(key)) {
            return stages.get(key);
        } else {
            throw new IllegalStateException(Main.I18N.get("warning.stage.unknown", key));
        }
    }

    public void registerView(Plugin key, String displayName, Node node, Position def) {
        if (tabViews.containsKey(key)) {
            throw new IllegalStateException(Main.I18N.get("warning.view.already_registered", key));
        } else {
            tabViews.put(key, createView(key, displayName, node, def));
        }
    }

    public void setViewVisible(Plugin key, boolean visible) {
        setViewVisible(key, visible, null);
    }

    public void setViewVisible(Plugin key, boolean visible, @Nullable Position position) {
        TabView view = getView(key);
        if (visible) {
            showView(view, position);
        } else {
            hideView(view);
        }
    }

    protected boolean isViewVisible(Plugin key) {
        TabView view = getView(key);
        return Arrays.stream(tabPanes).anyMatch(tabPane -> tabPane.getTabs().contains(view.getTab()));
    }

    protected void showView(TabView view, @Nullable Position position) {
        if (position == null) {
            position = view.getDef();
        }
        switch (position) {
            case LEFT:
                setViewVisible(view, leftTabs);
                break;
            case CENTER:
                setViewVisible(view, centerTabs);
                break;
            case RIGHT:
                setViewVisible(view, rightTabs);
                break;
            case BOTTOM:
                setViewVisible(view, bottomTabs);
                break;
        }
        setViewMenuStatus(view, true);
    }

    protected void hideView(TabView view) {
        Tab tab = view.getTab();
        for (TabPane tabPane : tabPanes) {
            if (tabPane.getTabs().removeIf(tab1 -> tab1.equals(tab))) {
                break;
            }
        }
        setViewMenuStatus(view, false);
    }

    protected void setViewMenuStatus(TabView view, boolean status) {
        setViewMenuStatus(view.getMenuItem(), status);
    }

    protected void setViewMenuStatus(MenuItem item, boolean shown) {
        String statusText = shown ? "\u2713" : "\u274C";
        if (item.getGraphic() instanceof Text) {
            Text text = (Text) item.getGraphic();
            text.setText(statusText);
        } else {
            item.setGraphic(new Text(statusText));
        }
    }

    public TabView getView(Plugin key) {
        if (tabViews.containsKey(key)) {
            return tabViews.get(key);
        } else {
            throw new IllegalStateException(Main.I18N.get("warning.view.unknown", key));
        }
    }

    protected void setViewVisible(TabView view, TabPane target) {
        Tab tab = view.getTab();
        target.getTabs().add(tab);
        target.getSelectionModel().select(tab);
    }

    protected TabView createView(Plugin key, String displayName, Node node, Position def) {
        Tab tab = new Tab();
        tab.setContent(node);
        tab.setOnClosed(event -> setViewMenuStatus(getView(key), false));
        final Label label = new Label(displayName);
        //label.setOnMouseClicked(event -> );
        tab.setGraphic(label);
        MenuItem menuItem = new MenuItem(displayName);
        menuItem.setOnAction(event -> setViewVisible(key, !isViewVisible(key)));
        mMainView.getItems().add(menuItem);
        return new TabView(tab, menuItem, def);
    }

    protected void setupTabPanes() {
        tabPanes = new TabPane[] { leftTabs, centerTabs, rightTabs, bottomTabs };
        for (TabPane tabPane : tabPanes) {
            tabPane.setRotateGraphic(true);
            //tabPane.setStyle("-fx-background-color: #DDDDDD; -fx-border-color: #191919");
        }
        new DraggingTabPaneSupport().addSupport(tabPanes);
        updateTabPaneSize(leftTabs, true, 0);
        updateTabPaneSize(rightTabs, true, 1);
        updateTabPaneSize(bottomTabs, false, -1);
        leftTabs.getTabs().addListener((ListChangeListener<? super Tab>) c ->
                updateTabPaneSize(leftTabs, true, 0));
        rightTabs.getTabs().addListener((ListChangeListener<? super Tab>) c ->
                updateTabPaneSize(rightTabs, true, 1));
        bottomTabs.getTabs().addListener((ListChangeListener<? super Tab>) c ->
                updateTabPaneSize(bottomTabs, false, 0));
    }

    protected void updateTabPaneSize(TabPane tabPane, boolean targetWidth, int divisorIdx) {
        if (targetWidth) {
            tabPane.setMinWidth(28.0d);
            if (tabPane.getTabs().size() > 0) {
                tabPane.setMaxWidth(Double.MAX_VALUE);
                tabPane.setPrefWidth(rootPane.getWidth()*0.2);
                double divisor = Math.abs(divisorIdx - 0.2);
                if (divisorIdx >= 0 &&
                        (divisorIdx == 0 && horSplitPane.getDividerPositions()[divisorIdx] < divisor
                        || divisorIdx == 1 && horSplitPane.getDividerPositions()[divisorIdx] > divisor)) {
                    horSplitPane.setDividerPosition(divisorIdx, divisor);
                }
            } else {
                tabPane.setMaxWidth(28.0d);
            }
        } else {
            tabPane.setMinHeight(28.0d);
            if (tabPane.getTabs().size() > 0) {
                tabPane.setMaxHeight(Double.MAX_VALUE);
                tabPane.setPrefHeight(rootPane.getHeight()*0.2);
                if (divisorIdx == 0 && vertSplitPane.getDividerPositions()[divisorIdx] > 0.8) {
                    vertSplitPane.setDividerPosition(divisorIdx, 0.8);
                }
            } else {
                tabPane.setMaxHeight(28.0d);
            }
        }
    }

    public void addFileMenu(String name, EventHandler<ActionEvent> onClick) {
        MenuItem item = new MenuItem(name);
        item.setOnAction(onClick);
        addFileMenu(item);
    }

    public void addFileMenu(MenuItem item) {
        mMainFile.getItems().add(item);
    }

    public void addHelpMenu(String name, EventHandler<ActionEvent> onClick) {
        MenuItem item = new MenuItem(name);
        item.setOnAction(onClick);
        addHelpMenu(item);
    }

    public void addHelpMenu(MenuItem item) {
        mMainHelp.getItems().add(item);
    }

    public Main getMain() {
        return main;
    }


    public static class TabView {
        private final Tab tab;
        private final Position def;
        private final MenuItem menuItem;

        public TabView(Tab tab, MenuItem menuItem, Position def) {
            this.tab = tab;
            this.menuItem = menuItem;
            this.def = def;
        }

        public Tab getTab() {
            return tab;
        }

        public MenuItem getMenuItem() {
            return menuItem;
        }

        public Position getDef() {
            return def;
        }
    }

    public enum Position {
        LEFT,
        CENTER,
        RIGHT,
        BOTTOM
    }
}
