package de.vaku.waypoint.controllers;

import de.vaku.waypoint.Main;
import javafx.scene.Parent;
import javafx.stage.Stage;

public class FXController<P extends Parent> {

    protected Main main;
    protected Stage rootStage;
    protected P root;

    public void init(Main main, Stage rootStage, P root) {
        this.main = main;
        this.rootStage = rootStage;
        this.root = root;
    }

    public P getRoot() {
        return root;
    }

    public Stage getRootStage() {
        return rootStage;
    }
}
