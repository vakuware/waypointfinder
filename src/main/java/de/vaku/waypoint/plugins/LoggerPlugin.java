package de.vaku.waypoint.plugins;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.utils.logging.TextFlowHandler;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.TextFlow;
import org.jetbrains.annotations.NotNull;

public class LoggerPlugin extends Plugin {

    public LoggerPlugin(RootController rootController) {
        super(rootController);
        TextFlow infoArea = new TextFlow();
        ScrollPane scrollPane = new ScrollPane(infoArea);
        rootController.registerView(this, Main.I18N.get("plugin.logger.logger"),
                scrollPane, RootController.Position.BOTTOM);
        rootController.setViewVisible(this, true);
        Main.LOGGER.addHandler(new TextFlowHandler(infoArea));
    }

    @Override
    public @NotNull String getName() {
        return "logger";
    }
}
