package de.vaku.waypoint.plugins;

import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.utils.saving.SaveMap;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public abstract class Plugin {

    protected final RootController rootController;
    protected final Map<Class<?>, Plugin> plugins;

    public Plugin(RootController rootController, Plugin... plugins) {
        this.rootController = rootController;
        this.plugins = new HashMap<>();
        for (Plugin plugin : plugins) {
            this.plugins.put(plugin.getClass(), plugin);
        }
    }

    /**
     * Called, when route is saved
     * @return Map of data
     */
    public SaveMap onSave(SaveMap save) {
        return save;
    }

    /**
     * Called, when route is loaded
     * @param data Map with data
     */
    public void onLoad(SaveMap data) {

    }

    /**
     * Called, when route is closed
     */
    public void onClose() {

    }

    @NotNull
    public abstract String getName();
}
