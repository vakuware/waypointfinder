package de.vaku.waypoint.plugins;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.utils.markdown.MarkdownUtils;
import javafx.geometry.Orientation;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.jetbrains.annotations.NotNull;

public class HelpPlugin extends Plugin {

    private final SplitPane splitPane;
    private final VBox items;
    private final ToggleGroup toggleGroup;
    private final TextFlow contentPane;

    public HelpPlugin(RootController rootController) {
        super(rootController);
        String text = Main.I18N.get("plugin.help.help");
        rootController.addHelpMenu(text, event -> rootController.setStageVisible(this, true));

        items = new VBox();
        toggleGroup = new ToggleGroup();
        contentPane = new TextFlow();
        splitPane = new SplitPane(new AnchorPane(items), new ScrollPane(contentPane));
        createWindow();

        Stage stage = rootController.registerStage(this, text, splitPane);
        stage.initStyle(StageStyle.UTILITY);
    }

    private void createWindow() {
        splitPane.setOrientation(Orientation.HORIZONTAL);
        AnchorPane.setLeftAnchor(items, 0.0d);
        AnchorPane.setTopAnchor(items, 0.0d);
        AnchorPane.setRightAnchor(items, 0.0d);
        AnchorPane.setBottomAnchor(items, 0.0d);
        splitPane.setDividerPosition(0, 0.2d);
    }

    public void registerItem(String displayName, String resPath) {
        ToggleButton button = new ToggleButton(displayName);
        button.setToggleGroup(toggleGroup);
        items.getChildren().add(button);
        button.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                contentPane.getChildren().addAll(MarkdownUtils.parseMarkdown(resPath));
            } else {
                contentPane.getChildren().clear();
            }
        });
        button.prefWidthProperty().bind(items.widthProperty());
        if (items.getChildren().size() == 1) {
            toggleGroup.selectToggle(button);
        }
    }

    @Override
    public @NotNull String getName() {
        return "help";
    }
}
