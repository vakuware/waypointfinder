package de.vaku.waypoint.plugins;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.plugins.route.RoutePlugin;
import de.vaku.waypoint.utils.saving.SaveMap;
import de.vaku.waypoint.utils.table.Row;
import de.vaku.waypoint.utils.table.Table;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WaypointPlugin extends Plugin {

    private final TableView<List<String>> tableView;
    private String tableName = "unknown";

    public WaypointPlugin(RootController rootController, RoutePlugin routePlugin, HelpPlugin helpPlugin) {
        super(rootController, routePlugin, helpPlugin);
        tableView = new TableView<>();
        rootController.registerView(this, Main.I18N.get("plugin.waypoint.waypoint"), tableView, RootController.Position.CENTER);
        rootController.setViewVisible(this, true);
        helpPlugin.registerItem("Waypoints", "lang/help_waypoints.md");
    }

    public void populateViewFromTable(Table table) {
        tableView.getColumns().clear();
        tableView.getItems().clear();
        List<String> columnNames = table.columns().stream().map(Table.Column::getHeader).collect(Collectors.toList());
        for (int i = 0; i < columnNames.size(); i++) {
            final int finalIdx = i;
            TableColumn<List<String>, String> column = new TableColumn<>(columnNames.get(i));
            column.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().get(finalIdx)));
            tableView.getColumns().add(column);
        }
        for (Row row : table) {
            tableView.getItems().add(row.asStringList());
        }
    }

    @Override
    public SaveMap onSave(SaveMap save) {
        Table table = new Table(tableName);
        for (TableColumn<List<String>, ?> column : tableView.getColumns()) {
            List<String> columnData = new ArrayList<>();
            for (List<String> item : tableView.getItems()) {
                columnData.add((String) column.getCellObservableValue(item).getValue());
            }
            table.insert(column.getText(), columnData);
        }
        save.put("waypoints", table);
        return save;
    }

    @Override
    public void onLoad(SaveMap data) {
        tableView.getColumns().clear();
        tableView.getItems().clear();
        if (data.contains("waypoints")) {
            Table table = data.get("waypoints", Table.class);
            populateViewFromTable(table);
            tableName = table.getName();
        }
    }

    @Override
    public @NotNull String getName() {
        return "waypoints";
    }
}
