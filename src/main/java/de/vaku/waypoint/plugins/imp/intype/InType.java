package de.vaku.waypoint.plugins.imp.intype;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.utils.table.Table;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;

public abstract class InType<T> {

    private final String extension;
    protected final Main main;

    public InType(String extension, Main main) {
        this.extension = extension;
        this.main = main;
    }

    public String getExtension() {
        return extension;
    }

    @Nullable
    public abstract Table getWaypointTable(File file);

    public abstract List<Object> getWaypoint(T waypointNode);
}
