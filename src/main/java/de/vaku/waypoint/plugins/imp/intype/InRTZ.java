package de.vaku.waypoint.plugins.imp.intype;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.utils.table.Table;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InRTZ extends InType<Node> {

    public InRTZ(Main main) {
        super("rtz", main);
    }

    @Override
    public @Nullable Table getWaypointTable(File file) {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);
            doc.getDocumentElement().normalize();
        } catch (IOException | SAXException | ParserConfigurationException e) {
            main.appendStatusText("Could not parse selected file - are you sure its the correct format?");
        }
        if (doc != null) {
            try {
                String routeName = doc.getElementsByTagName("routeInfo").item(0)
                        .getAttributes().getNamedItem("routeName").getNodeValue();
                NodeList waypointsContent = doc.getElementsByTagName("waypoints").item(0).getChildNodes();

                Table table = new Table(routeName);
                table.insert("id", Long.class);
                table.insert("name", String.class);
                table.insert("lat", Double.class);
                table.insert("lon", Double.class);
                table.insert("geom", String.class);
                table.insert("speed", Double.class);

                for (int wId = 0; wId < waypointsContent.getLength(); wId++) {
                    if (waypointsContent.item(wId).getNodeName().equals("waypoint")) {
                        table.add(getWaypoint(waypointsContent.item(wId)));
                    }
                }
                return table;
            } catch (Exception e) {
                main.appendStatusText("Error while reading the file - probably the structure of your file was different than expected");
                return null;
            }
        }
        return null;
    }

    public List<Object> getWaypoint(Node waypointNode) {
        List<Object> row = new ArrayList<>();

        NamedNodeMap waypointAttr = waypointNode.getAttributes();
        row.add(Long.parseLong(waypointAttr.getNamedItem("id").getNodeValue()));
        row.add(waypointAttr.getNamedItem("name").getNodeValue());
        Double lat = null;
        Double lon = null;
        String geom = null;
        Double speed = null;
        for (int i = 0; i < waypointNode.getChildNodes().getLength(); i++) {
            Node node = waypointNode.getChildNodes().item(i);
            if (node.getNodeName().equals("position")) {
                NamedNodeMap positionAttr = node.getAttributes();
                lat = Double.parseDouble(positionAttr.getNamedItem("lat").getNodeValue());
                lon = Double.parseDouble(positionAttr.getNamedItem("lon").getNodeValue());
            }
            if (node.getNodeName().equals("leg")) {
                NamedNodeMap positionAttr = node.getAttributes();
                String geometryType = positionAttr.getNamedItem("geometryType").getNodeValue();
                if (geometryType.equals("Loxodrome")) {
                    geom = "RL";
                } else if (geometryType.equals("Orthodrome")) {
                    geom = "GC";
                }
                speed = Double.parseDouble(positionAttr.getNamedItem("speedMax").getNodeValue());
            }
        }
        row.addAll(Arrays.asList(lat, lon, geom, speed));
        return row;
    }
}
