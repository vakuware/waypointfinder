package de.vaku.waypoint.plugins.imp;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.plugins.Plugin;
import de.vaku.waypoint.plugins.route.RoutePlugin;
import de.vaku.waypoint.plugins.WaypointPlugin;
import de.vaku.waypoint.plugins.imp.intype.InType;
import de.vaku.waypoint.utils.saving.SaveFile;
import de.vaku.waypoint.utils.saving.SaveMap;
import de.vaku.waypoint.utils.table.Table;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ImportPlugin extends Plugin {
    protected final List<InType<?>> inTypes;
    protected final RoutePlugin routePlugin;
    protected final WaypointPlugin waypointPlugin;

    public ImportPlugin(RootController rootController, RoutePlugin routePlugin, WaypointPlugin waypointPlugin) {
        super(rootController, routePlugin, waypointPlugin);
        this.inTypes = new ArrayList<>();
        this.routePlugin = routePlugin;
        this.waypointPlugin = waypointPlugin;
        rootController.addFileMenu(Main.I18N.get("plugin.import.import"), this::onImport);
    }

    public ImportPlugin registerInType(InType<?> inType) {
        inTypes.add(inType);
        return this;
    }

    public List<String> getExtensions() {
        return inTypes.stream().map(inType -> "*." + inType.getExtension()).collect(Collectors.toList());
    }

    private void onImport(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        FileChooser.ExtensionFilter mainFilter = new FileChooser.ExtensionFilter(
                Main.I18N.get("plugin.import.filter.coord"), getExtensions());
        FileChooser.ExtensionFilter allFilter = new FileChooser.ExtensionFilter(
                Main.I18N.get("file.filter.all"), "*");
        fileChooser.getExtensionFilters().add(mainFilter);
        fileChooser.getExtensionFilters().add(allFilter);
        fileChooser.setSelectedExtensionFilter(mainFilter);
        File file = fileChooser.showOpenDialog(rootController.getRootStage());
        if (file != null) {
            String extensionName = FilenameUtils.getExtension(file.getName());
            Optional<InType<?>> opt = inTypes.stream().filter(inType1 ->
                    inType1.getExtension().equals(extensionName)).findFirst();
            if (opt.isPresent()) {
                SaveFile saveFile = importRouteFromWP(file, opt.get());
                if (saveFile != null) {
                    if (routePlugin.open(saveFile)) {
                        Main.LOGGER.info(Main.I18N.get("file.import.success", file.getName()));
                    } else {
                        Main.LOGGER.warning(Main.I18N.get("file.import.failure", file.getName()));
                    }
                } else {
                    Main.LOGGER.warning(Main.I18N.get("file.import.no_valid_file", file.getName()));
                }
            } else {
                Main.LOGGER.warning(Main.I18N.get("file.import.unsupported_ext", extensionName));
            }
        }
    }

    @Nullable
    public SaveFile importRouteFromWP(@NotNull File file, InType<?> inType) {
        Table waypointTable = inType.getWaypointTable(file);
        if (waypointTable != null) {
            SaveFile saveFile = new SaveFile(waypointTable.getName());
            List<SaveFile.Entry> entries = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            map.put("waypoints", waypointTable);
            entries.add(SaveFile.fromYaml(waypointPlugin.getName(), new SaveMap(map)));
            saveFile.setEntries(entries);
            if (saveFile.save()) {
                return saveFile;
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public @NotNull String getName() {
        return "import";
    }
}
