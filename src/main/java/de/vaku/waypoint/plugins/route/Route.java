package de.vaku.waypoint.plugins.route;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.plugins.Plugin;
import de.vaku.waypoint.utils.saving.SaveMap;
import de.vaku.waypoint.utils.saving.SaveFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Route(name)
 * save()
 */
public class Route {
    private final String name;
    private final SaveFile saveFile;

    public Route(String name) {
        this.name = name;
        this.saveFile = new SaveFile(name);
    }

    public Route(SaveFile saveFile) {
        this.saveFile = saveFile;
        this.name = saveFile.getFile().getName();
    }

    boolean save() {
        List<SaveFile.Entry> entries = new ArrayList<>();
        Main.getPlugins().forEach(plugin -> {
            SaveMap res = plugin.onSave(new SaveMap());
            if (!res.getMap().isEmpty()) {
                entries.add(SaveFile.fromYaml(plugin.getName(), res));
            }
        });
        saveFile.setEntries(entries);
        return saveFile.save();
    }

    void load() {
        for (Plugin plugin : Main.getPlugins()) {
            SaveMap saveMap = saveFile.getSaveMap(plugin.getName());
            if (saveMap != null) {
                plugin.onLoad(saveMap);
            } else {
                plugin.onLoad(new SaveMap());
            }
        }
        Main.LOGGER.info(Main.I18N.get("plugin.route.loaded", name));
    }

    void close() {
        Main.getPlugins().forEach(Plugin::onClose);
    }

    public String getName() {
        return name;
    }

}
