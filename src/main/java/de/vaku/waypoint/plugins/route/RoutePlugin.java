package de.vaku.waypoint.plugins.route;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.plugins.HelpPlugin;
import de.vaku.waypoint.plugins.Plugin;
import de.vaku.waypoint.utils.saving.SaveFile;
import javafx.stage.FileChooser;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collections;

public class RoutePlugin extends Plugin {

    private Route route;

    public RoutePlugin(RootController rootController, HelpPlugin helpPlugin) {
        super(rootController, helpPlugin);
        rootController.addFileMenu(Main.I18N.get("plugin.route.open"), event -> open());
        rootController.addFileMenu(Main.I18N.get("plugin.route.save"), event -> save());

        helpPlugin.registerItem("Base", "lang/help_base.md");
    }

    public void set(@NotNull Route route) {
        closeCurrent();
        this.route = route;
        route.load();
    }

    public void closeCurrent() {
        if (this.route != null) {
            route.close();
            route.save();
        }
    }

    public void save() {
        if (route != null) {
            if (route.save()) {
                Main.LOGGER.info(Main.I18N.get("plugin.route.save_successful", route.getName()));
            } else {
                Main.LOGGER.severe(Main.I18N.get("plugin.route.save_failed", route.getName()));
            }
        } else {
            Main.LOGGER.warning(Main.I18N.get("plugin.route.save_no_route"));
        }
    }

    public boolean open() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        FileChooser.ExtensionFilter mainFilter = new FileChooser.ExtensionFilter(
                Main.I18N.get("plugin.import.filter.route"), Collections.singletonList("*." + SaveFile.EXTENSION));
        FileChooser.ExtensionFilter allFilter = new FileChooser.ExtensionFilter(
                Main.I18N.get("file.filter.all"), "*");
        fileChooser.getExtensionFilters().add(mainFilter);
        fileChooser.getExtensionFilters().add(allFilter);
        fileChooser.setSelectedExtensionFilter(mainFilter);
        File file = fileChooser.showOpenDialog(rootController.getRootStage());
        if (file != null) {
            return open(new SaveFile(file));
        }
        return false;
    }

    public boolean open(SaveFile saveFile) {
        if (saveFile.load()) {
            set(new Route(saveFile));
            Main.LOGGER.info(Main.I18N.get("plugin.route.open_successful", saveFile.getFile().getName()));
            return true;
        } else {
            Main.LOGGER.warning(Main.I18N.get("plugin.route.open_failed", saveFile.getFile().getName()));
            return false;
        }
    }

    @Override
    public @NotNull String getName() {
        return "route";
    }
}
