package de.vaku.waypoint;

import de.vaku.waypoint.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Waypoint {
    public static final String NAN = "N/A";
    public static double DEGREE_THRESH = 2d * Math.PI/180;

    public List<Feature<?>> features = new ArrayList<>();
    public final Feature<Long> id = new Feature<>(this,"ID", 5);
    public final StringFeature name = new StringFeature(this,"NAME", 15);
    public DoubleFeature lat = new DoubleFeature(this,"LAT", 13, 2, 3);
    public DoubleFeature lon = new DoubleFeature(this,"LON", 14, 3, 3);
    public DoubleFeature remain = new DoubleFeature(this,"REMAIN", 10, 0, 2, true);
    public DoubleFeature length = new DoubleFeature(this,"LEN", 8, 0, 2, true);
    public DoubleFeature crs = new DoubleFeature(this,"CRS", 7, 3, 1);
    public StringFeature geomType = new StringFeature(this,"M", 4);
    public DoubleFeature maxSpeed = new DoubleFeature(this,"SPD", 5, 2, 1);

    public Waypoint() {

    }

    public void calcCourse(Waypoint lastWaypoint) {
        if ((lastWaypoint.lat == null) || (lastWaypoint.lon == null) || (lat == null) || (lon == null)) {
            return;
        }
        double lA = Math.PI / 180 * lastWaypoint.lon.getValue();
        double lB = Math.PI / 180 * lon.getValue();
        double pA = Math.PI / 180 * lastWaypoint.lat.getValue();
        double pB = Math.PI / 180 * lat.getValue();
        double length;
        double crs;
        if (geomType.getValue().equals("GC")) {
            length = 60 * Math.acos(Math.sin(pA)*Math.sin(pB) + Math.cos(pA)*Math.cos(pB)*Math.cos(lB - lA));
        } else {
            crs = Math.atan((lB - lA) / (Math.log(Math.tan(Math.PI / 4 + pB / 2)) - Math.log(Math.tan(Math.PI / 4 + pA / 2))));
            if (pB - pA < 0) {
                crs += Math.PI;
            } else if (lB - lA < 0) {
                crs += 2 * Math.PI;
            }
            if (Math.abs(Math.PI / 2 - crs) < DEGREE_THRESH || Math.abs(3 * Math.PI / 2 - crs) < DEGREE_THRESH) {
                length = 60 * Math.abs(lB - lA) * Math.cos((pA + pB) / 2);
            } else {
                length = 60 * (pB - pA) / Math.cos(crs);
            }

            this.crs.setValue(crs * 180 / Math.PI);
        }
        this.length.setValue(length * 180 / Math.PI);
    }

    @Override
    public String toString() {
        return features.stream().map(Feature::getRepresentation).collect(Collectors.joining(","));
        //return createCommaString(id, name, lat, lon, length, remain, crs, geomType, maxSpeed);
    }

    public String getDMS(Double dec, String dirPos, String dirNeg, int nums) {
        if (dec == null) {
            return NAN;
        }
        long degree = (long) Math.abs(dec);
        String minutes = "" + Utils.round((Math.abs(dec) - degree) * 60, 3);
        minutes = Utils.fixDecLength(minutes, 3);
        minutes = Utils.fixNumLength(minutes, 2);
        String res = Utils.fixNumLength("" + degree, nums) + "°" + minutes + "' ";
        if (dec > 0) {
            res += dirPos;
        } else {
            res += dirNeg;
        }
        return res;
    }

    public List<Feature<?>> getFeatures() {
        return features;
    }

    public static class Feature<T> {
        protected final String key;
        protected T value;
        protected String representation;
        protected final int maxChars;
        protected final boolean right;

        public Feature(Waypoint wp, String key, int maxChars, boolean right) {
            wp.features.add(this);
            this.key = key;
            this.value = null;
            this.maxChars = maxChars;
            this.right = right;
        }

        public Feature(Waypoint wp, String key, int maxChars) {
            this(wp, key, maxChars, false);
        }

        public void setValue(T value) {
            setValue(value, Object::toString);
        }

        public void setValue(T value, Function<T, String> repFunction) {
            this.value = value;
            this.representation = repFunction.apply(value);
        }

        public String getRepresentation() {
            return checkedString(representation);
        }

        public boolean hasValue() {
            return this.value != null;
        }


        public T getValue() {
            return value;
        }

        protected String checkedString(String value) {
            if (value == null) {
                return NAN;
            }
            return value.codePoints().limit(maxChars)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint,StringBuilder::append)
                    .toString();
        }

        public String getKey() {
            return key;
        }

        public int getMaxChars() {
            return maxChars;
        }

        public boolean isAlignedRight() {
            return right;
        }
    }

    public static class StringFeature extends Feature<String> {
        public StringFeature(Waypoint wp, String key, int maxChars, boolean right) {
            super(wp, key, maxChars, right);
        }

        public StringFeature(Waypoint wp, String key, int maxChars) {
            this(wp, key, maxChars, false);
        }
    }

    public static class DoubleFeature extends Feature<Double> {
        public final int numInts;
        public final int numDecs;

        public DoubleFeature(Waypoint wp, String key, int maxChars, int numInts, int numDecs, boolean right) {
            super(wp, key, maxChars, right);

            this.numInts = numInts;
            this.numDecs = numDecs;
        }

        public DoubleFeature(Waypoint wp, String key, int maxChars, int numInts, int numDecs) {
            this(wp, key, maxChars, numInts, numDecs, false);
        }

        @Override
        public void setValue(Double value) {
            setValue(value, val -> Utils.round(val, numDecs).toString());
        }


        @Override
        public void setValue(Double value, Function<Double, String> repFunction) {
            this.value = value;
            this.representation = Utils.fixNumLength(Utils.fixDecLength(
                    repFunction.apply(value), numDecs), numInts);
        }
    }
}