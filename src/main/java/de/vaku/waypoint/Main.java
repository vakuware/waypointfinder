package de.vaku.waypoint;

import de.vaku.waypoint.controllers.RootController;
import de.vaku.waypoint.plugins.HelpPlugin;
import de.vaku.waypoint.plugins.LoggerPlugin;
import de.vaku.waypoint.plugins.route.RoutePlugin;
import de.vaku.waypoint.plugins.WaypointPlugin;
import de.vaku.waypoint.plugins.imp.ImportPlugin;
import de.vaku.waypoint.plugins.imp.intype.InRTZ;
import de.vaku.waypoint.outtype.OutType;
import de.vaku.waypoint.plugins.Plugin;
import de.vaku.waypoint.utils.I18N;
import de.vaku.waypoint.utils.Utils;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {
    public static final int MAX_CHARS = 81;

    public static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    public static final I18N I18N = new I18N("base");
    protected JPanel footRadioPanel;

    protected final Map<JRadioButton, OutType> outTypes;
    protected ButtonGroup outTypeGroup;
    protected WaypointFile waypointFile;

    private RootController rootController;
    private static final Map<Class<?>, Plugin> plugins = new HashMap<>();

    public static void main(String[] args) {
        LOGGER.setLevel(Level.ALL);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        rootController = Utils.loadFXML("fxml/rootView.fxml", this, primaryStage);
        primaryStage.setScene(new Scene(rootController.getRoot(), 800, 450));
        primaryStage.setTitle("Waypointfinder");
        //primaryStage.initStyle(StageStyle.TRANSPARENT);
        //primaryStage.setResizable(true);
        //rootController.getRoot().setStyle("-fx-background-color:transparent;");
        primaryStage.show();
        HelpPlugin helpPlugin = registerPlugin(new HelpPlugin(rootController));
        RoutePlugin routePlugin = registerPlugin(new RoutePlugin(rootController, helpPlugin));
        registerPlugin(new LoggerPlugin(rootController));
        WaypointPlugin waypointPlugin = registerPlugin(new WaypointPlugin(rootController, routePlugin, helpPlugin));
        registerPlugin(new ImportPlugin(rootController, routePlugin, waypointPlugin))
                .registerInType(new InRTZ(this));



        Main.LOGGER.config(Main.I18N.get("config.view_loaded"));
    }

    public static <P extends Plugin> P registerPlugin(P plugin) {
        Class<? extends Plugin> clazz = plugin.getClass();
        if (plugins.containsKey(clazz)) {
            throw new IllegalStateException(I18N.get("warning.plugin.already_registered", clazz));
        } else {
            plugins.put(clazz, plugin);
        }
        return plugin;
    }

    public static <P extends Plugin> P getPlugin(Class<P> clazz) {
        if (plugins.containsKey(clazz)) {
            return clazz.cast(plugins.get(clazz));
        } else {
            throw new IllegalStateException(I18N.get("warning.plugin.unknown", clazz));
        }
    }

    public static Collection<Plugin> getPlugins() {
        return plugins.values();
    }

    public RootController getRootController() {
        return rootController;
    }

    public Main() {
        outTypes = new HashMap<>();
        //addContent();

        //registerOutType(new OutPDF());
        //registerOutType(new OutTXT());
        //registerOutType(new OutCSV());
    }

    /*
        protected void addContent() {
            this.setTitle("Hello :) Im gonna convert your file!");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setSize(450, 400);
            this.setLayout(new BorderLayout());

            statusTextArea = new JTextArea();
            appendStatusText("Choose a source file first ...");
            statusTextArea.setLineWrap(true);
            statusTextArea.setBounds(0,0,600, 200);
            this.getContentPane().add(statusTextArea, BorderLayout.CENTER);
            chooseFileButton = new JButton("Choose file");
            chooseFileButton.setBounds(0,200,600, 100);
            chooseFileButton.addActionListener(this);
            this.getContentPane().add(chooseFileButton, BorderLayout.LINE_START);
            convertButton = new JButton("Start conversion");
            convertButton.setBounds(0,300,600, 100);
            convertButton.addActionListener(this);

            this.getContentPane().add(convertButton, BorderLayout.LINE_END);

            outTypeGroup = new ButtonGroup();
            helpButton = new JButton("help");
            helpButton.addActionListener(this);
            footRadioPanel = new JPanel();
            footRadioPanel.setLayout(new GridLayout(1, 2));
            footRadioPanel.add(helpButton);
            this.add(footRadioPanel, BorderLayout.SOUTH);
        }


        public void actionPerformed(ActionEvent e) {
            Object source = e.getSource();
            if (source.equals(chooseFileButton)) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                fileChooser.setFileFilter(getFileFilter());
                int result = fileChooser.showOpenDialog(this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selected = fileChooser.getSelectedFile();
                    String extensionName = FilenameUtils.getExtension(selected.getName());
                    Optional<InType<?>> opt = inTypes.stream().filter(inType1 -> inType1.getExtension().equals(extensionName)).findFirst();
                    if (opt.isPresent()) {
                        waypointFile.setFile(selected, opt.get());
                    } else {
                        appendStatusText("File " + selected.getName() + " is of unknown fileType and could not be selected");
                    }
                }
            } else if (source.equals(convertButton)) {
                waypointFile.writeToFile();
            } else if (source.equals(helpButton)) {
                Object[] options = {"<HTML><U>Gitlab repo</U></HTML>", "Cancel"};
                int res = JOptionPane.showOptionDialog(this,
                        Utils.readResFile("help.html")
                                .replace("%chooseFile%", chooseFileButton.getText())
                                .replace("%convertFile%", convertButton.getText())
                                .replace("%outTypes%", "'" + outTypes.values().stream()
                                        .map(OutType::getExtension).collect(Collectors.joining("', '")) + "'")
                                .replace("%inTypes%", "'" + inTypes.stream()
                                        .map(InType::getExtension).collect(Collectors.joining("', '")) + "'")
                                .replace("%gitlab%", GITLAB_URL),
                        "Help", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);
                if (res == 0) {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(URI.create(GITLAB_URL));
                        } catch (IOException ex) {
                            appendStatusText("Could not open URL " + GITLAB_URL);
                        }
                    } else {
                        appendStatusText("Could not open URL " + GITLAB_URL);
                    }
                }
            } else if(source instanceof JRadioButton && outTypes.containsKey(source)) {
                waypointFile.setOutType(outTypes.get(source));
            }
        }

        public void appendStatusText(String str) {
            statusTextArea.append(str + "\n");
        }
     */
    public void registerOutType(OutType outType) {
        JRadioButton csvButton = new JRadioButton(outType.getExtension());
        csvButton.setToolTipText(outType.getDescription());
        //csvButton.addActionListener(this);
        outTypeGroup.add(csvButton);
        footRadioPanel.add(csvButton);
        outTypes.put(csvButton, outType);
        if (outTypes.size() == 1) {
            csvButton.setSelected(true);
            waypointFile.setOutType(outType);
        }
    }

    public void appendStatusText(String s) {
    }

}
