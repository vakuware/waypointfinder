package de.vaku.waypoint.utils.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Row extends ArrayList<Object> {
    public static String NULL_STRING = "Null";

    public Row() {

    }

    public Row(Collection<?> collection) {
        addAll(collection);
    }

    public List<String> asStringList() {
        return stream().map(o -> {
            if (o != null) {
                return o.toString();
            }
            return NULL_STRING;
        }).collect(Collectors.toList());
    }
}
