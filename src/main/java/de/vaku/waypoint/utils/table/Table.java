package de.vaku.waypoint.utils.table;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Usage:
 * for (Row row : table) { print(row) }
 * table.add(row)
 * table.get("colName")
 * table.insert("colName", ListOfData)
 * table.has("colName")
 * table.headers()
 */
public class Table extends ArrayList<Row> {

    private Map<String, Column<?>> columns;
    private String name;

    /**
     * Returns new, empty table with unspecified name
     */
    public Table() {
        this("unnamed");
    }

    /**
     * Returns new, empty table
     * @param name Name of Table
     */
    public Table(String name) {
        columns = new HashMap<>();
        this.name = name;
    }

    /**
     * Inserts new, filled column
     * @param header Name of column
     * @param colData Data of column which will be filled in rows
     * @param <T> Type of Data
     * @return Returns if column could be inserted
     */
    @SuppressWarnings("unchecked")
    public final <T> boolean insert(@NotNull String header, List<T> colData) {
        if (colData.size() == 0) {
            System.err.println("Could not add Column. Data for column " + header + " has to be specified");
            return false;
        }
        Class<?> clazz = Object.class;
        for (T colDatum : colData) {
            if (colDatum != null) {
                clazz = colDatum.getClass();
                break;
            }
        }
        return insert(header, (Class<T>)clazz, colData);
    }

    /**
     * Inserts new, filled column
     * @param header Name of column
     * @param type Class of DataType
     * @param colData Data of column which will be filled in rows
     * @param <T> Type of Data
     * @return Returns if column could be inserted
     */
    public final <T> boolean insert(@NotNull String header, @NotNull Class<T> type, List<T> colData) {
        if (size() > 0 && colData.size() != size()) {
            System.err.println("Could not add Column. Data length for column " + header + " of size " + colData.size() +
                    " has to match tables size of " + get(0).size());
            return false;
        }
        if (!insert(header, type)) {
            return false;
        }
        int index = columns.get(header).getIndex();
        for (int i = 0; i < colData.size(); i++) {
            if (i >= size()) {
                Row row = new Row();
                row.add(colData.get(i));
                add(row);
            } else {
                get(i).set(index, colData.get(i));
            }
        }
        return true;
    }

    /**
     * Inserts new, empty(null) column
     * @param header Name of column
     * @param type Class of DataType
     * @param <T> Type of Data
     * @return Returns if column could be inserted
     */
    public final <T> boolean insert(@NotNull String header, @NotNull Class<T> type) {
        if (has(header)) {
            System.err.println("Could not add Column. Column " + header + " is already present.");
            return false;
        }
        columns.put(header, new Column<>(header, type, columns.size()));
        for (int i = 0; i < size(); i++) {
            get(i).add(null);
        }
        return true;
    }

    /**
     * Checks if column is defined
     * @param header Name of column
     * @return boolean if present
     */
    public boolean has(String header) {
        return columns.containsKey(header);
    }

    /**
     * Add new row
     * @param row List of row to add
     * @return success
     */
    public boolean add(List<?> row) {
        return add(new Row(row));
    }

    /**
     * Add new row
     * @param row Row row to add
     * @return success
     */
    @Override
    public boolean add(Row row) {
        if (!isRowValid(row)) {
            return false;
        }
        return super.add(row);
    }

    /**
     * Checks if row is valid in size and datatype
     * @param row Row row
     * @return success
     */
    public boolean isRowValid(Row row) {
        if (row.size() != columns.size()) {
            System.err.println("Could not add Row. Number of items " + row.size() + " has to match number of columns " + columns.size());
            return false;
        }
        for (Column<?> column : columns.values()) {
            int i = column.getIndex();
            Object obj = row.get(i);
            Class<?> type = column.getType();
            if (obj != null && !obj.getClass().isAssignableFrom(type)) {
                System.err.println("Could not add Item. Items type " + obj.getClass() + " did not match the columns type " + type);
                return false;
            }
        }
        return true;
    }

    /**
     * Returns rows between start and end index
     * @param start Start index
     * @param end End index
     * @return List of rows
     */
    public List<Row> get(int start, int end) {
        List<Row> rows = new ArrayList<>();
        for (int i = start; i < end; i++) {
            rows.add(get(i));
        }
        return rows;
    }

    /**
     * Returns all items of column
     * @param header Column header
     * @param <T> Column data type
     * @return List of items
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> get(String header) {
        if (!has(header)) {
            System.err.println("Column " + header + " is not defined");
            return new ArrayList<>();
        }
        int index = columns.get(header).getIndex();
        List<Object> result = new ArrayList<>();
        for (Row row : this) {
            result.add(row.get(index));
        }
        return (List<T>) result;
    }

    /**
     * Removes column and all its items
     * @param header Column header
     * @return success
     */
    public boolean remove(String header) {
        if (!has(header)) {
            System.err.println("Column " + header + " is not defined");
            return false;
        }
        int index = columns.get(header).getIndex();
        for (Row row : this) {
            row.remove(index);
        }
        columns.remove(header);
        return true;
    }

    /**
     * Returns all column definitions
     * @return List of columns
     */
    public List<Column<?>> columns() {
        return columns.values().stream().sorted(Comparator.comparing(Column::getIndex)).collect(Collectors.toList());
    }

    /**
     * Returns name of table
     * @return name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Table ").append(name).append("\n");
        builder.append(columns())
                .append("\n");
        for (Row row : this) {
            builder.append(row.toString())
                    .append("\n");
        }
        return builder.toString();
    }

    public Object clone() {
        Table clone = (Table) super.clone();
        clone.columns = columns;
        clone.name = name;
        return clone;
    }

    public static class Column<T> {
        private final String header;
        private final Class<T> type;
        private final int index;

        public Column(@NotNull String header, @NotNull Class<T> type, int index) {
            this.header = header;
            this.type = type;
            this.index = index;
        }

        @NotNull
        public String getHeader() {
            return header;
        }

        @NotNull
        public Class<T> getType() {
            return type;
        }

        public int getIndex() {
            return index;
        }

        @Override
        public String toString() {
            return header + "<" + index + ", " + type.getSimpleName() + ">";
        }
    }
}
