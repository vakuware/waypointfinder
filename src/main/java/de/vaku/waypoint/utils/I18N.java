package de.vaku.waypoint.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public class I18N {
    public static final String PATH = "lang";
    private final String baseName;
    private ResourceBundle resourceBundle;
    public I18N(String baseName) {
        this.baseName = baseName;
        setLocale(Locale.getDefault());
    }

    public void setLocale(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(PATH + "/" + baseName, locale);
    }

    public String get(String key, Object... args) {
        return String.format(resourceBundle.getString(key), args);
    }

    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }
}
