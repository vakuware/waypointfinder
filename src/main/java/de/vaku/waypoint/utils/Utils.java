package de.vaku.waypoint.utils;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.controllers.FXController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Utils {

    public static Double round(Double value, int decs) {
        if (value == null) {
            return value;
        }
        double factor = Math.pow(10, decs);
        return (double) Math.round(value * factor) / factor;
    }

    public static String fixDecLength(String str, int decs) {
        if (str == null) {
            return null;
        }
        StringBuilder strBuilder = new StringBuilder(str);
        for (int i = str.substring(str.indexOf('.')).length(); i < decs + 1; i++) {
            strBuilder.append('0');
        }
        str = strBuilder.toString();
        return str;
    }

    public static String fixNumLength(String str, int nums) {
        if (str == null) {
            return null;
        }
        int len;
        if (str.indexOf('.') > 0) {
            len = str.substring(0, str.indexOf('.')).length();
        } else {
            len = str.length();
        }
        StringBuilder strBuilder = new StringBuilder(str);
        for (int i = 0; i < nums - len; i++) {
            strBuilder.insert(0, '0');
        }
        str = strBuilder.toString();
        return str;
    }

    public static String repeat(String s, int n) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            builder.append(s);
        }
        return builder.toString();
    }

    public static String readFile(Scanner scanner, String lineEnd) {
        StringBuilder result = new StringBuilder();
        while (scanner.hasNextLine()) {
            result.append(scanner.nextLine());
            if (scanner.hasNextLine())
                result.append(lineEnd);
        }
        return result.toString();
    }

    public static String readResFile(String resPath, String lineEnd) {
        InputStream stream = Utils.class.getClassLoader().getResourceAsStream(resPath);

        if (stream != null) {
            return readFile(new Scanner(stream).useDelimiter("\\A"), lineEnd);
        } else {
            return "Could not open resource " + resPath;
        }
    }

    public static String readResFile(String resPath) {
        return readResFile(resPath, "");
    }

    @Nullable
    public static URL loadFXML(String resName) {
        URL fxml = Utils.class.getClassLoader().getResource(resName);
        if (fxml == null) {
            Main.LOGGER.config(Main.I18N.get("warning.unknown_fxml", resName));
            return null;
        }
        return fxml;
    }

    public static <C extends FXController<P>, P extends Parent> C loadFXML(String resName, Main main, Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(loadFXML(resName));
        loader.setResources(Main.I18N.getResourceBundle());
        P root = loader.load();

        if (root != null) {
            C controller = loader.getController();
            controller.init(main, primaryStage, root);
            return controller;
        } else {
            throw new IllegalStateException(Main.I18N.get("warning.unknown_fxml", resName));
        }
    }
}