package de.vaku.waypoint.utils.logging;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class TextFlowHandler extends Handler {
    public static final Map<Level, Color> LEVEL_COLOR_MAP;

    static {
        LEVEL_COLOR_MAP = new HashMap<>();
        LEVEL_COLOR_MAP.put(Level.SEVERE, Color.DARKRED);
        LEVEL_COLOR_MAP.put(Level.WARNING, Color.ORANGE);
        LEVEL_COLOR_MAP.put(Level.INFO, Color.BLACK);
        LEVEL_COLOR_MAP.put(Level.CONFIG, Color.DARKGREEN);
        LEVEL_COLOR_MAP.put(Level.FINE, Color.GREEN);
        LEVEL_COLOR_MAP.put(Level.FINER, Color.LIME);
    }

    private final TextFlow target;

    public TextFlowHandler(TextFlow target) {
        this.target = target;
    }

    @Override
    public void publish(LogRecord record) {
        Text text = new Text(record.getMessage() + "\n");
        text.setFill(LEVEL_COLOR_MAP.getOrDefault(record.getLevel(), Color.BLACK));
        target.getChildren().add(text);
    }

    @Override
    public void flush() {

    }

    @Override
    public void close() throws SecurityException {

    }
}
