package de.vaku.waypoint.utils.saving;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class SaveFile {

    public static final SaveVersion CURRENT_VERSION = SaveVersion.V_1_0;
    public static final String EXTENSION = "route";

    private static final CustomConstructor constructor = new CustomConstructor();
    private static final CustomRepresenter representer = new CustomRepresenter();
    private static final Yaml yaml = getYaml();
    private static final ParserToString YAML_TO_STRING = saveMap -> yaml.dump(saveMap.getMap());
    private static final ParserFromString YAML_FROM_STRING = stream -> new SaveMap(yaml.load(stream));
    private final File file;
    private final List<Entry> entries;

    public SaveFile(String path) {
        file = new File(path + "." + EXTENSION);
        entries = new ArrayList<>();
        entries.add(getVersionEntry());
    }

    public SaveFile(@NotNull File file) {
        this.file = file;
        entries = new ArrayList<>();
    }

    public void setEntries(List<Entry> entries) {
        this.entries.clear();
        this.entries.add(getVersionEntry());
        this.entries.addAll(entries);
    }

    public boolean save() {
        try {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(file));
            for (Entry entry : entries) {
                out.putNextEntry(new ZipEntry(entry.getName() + "." + entry.getExtension()));
                byte[] data = entry.getBytes();
                out.write(data, 0, data.length);
                out.closeEntry();
            }
            out.close();
            return true;
        } catch (IOException ex) {
            System.err.println("Could not save file - is it busy?");
            return false;
        }
    }

    public boolean load() {
        try {
            ZipFile zipFile = new ZipFile(file);
            zipFile.stream().forEach(zipEntry -> {
                String name = FilenameUtils.getBaseName(zipEntry.getName());
                String extension = FilenameUtils.getExtension(zipEntry.getName());
                if (extension.equals("yaml")) {
                    try {
                        entries.add(Entry.of(name, extension, YAML_FROM_STRING.apply(zipFile.getInputStream(zipEntry)), YAML_TO_STRING));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.err.println("Unknown extension " + extension + " for " + name);
                }
            });
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Nullable
    public SaveMap getSaveMap(String entryName) {
        Optional<Entry> opt = entries.stream().filter(entry -> entry.getName().equals(entryName)).findFirst();
        return opt.map(Entry::getMap).orElse(null);
    }

    public static Entry getVersionEntry() {
        SaveMap versions = new SaveMap();
        versions.put("version", CURRENT_VERSION.VERSION);
        versions.put("sub_version", CURRENT_VERSION.SUB_VERSION);
        return fromYaml("version", versions);
    }

    public static Entry fromYaml(String name, SaveMap saveMap) {
        return Entry.of(name, "yaml", saveMap, YAML_TO_STRING);
    }

    private static Yaml getYaml() {
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        return new Yaml(constructor, representer, options);
    }

    public File getFile() {
        return file;
    }

    public static class Entry {
        private final String name;
        private final String extension;
        private final SaveMap map;
        private final ParserToString parserTo;

        private Entry (String name, String extension, SaveMap map, ParserToString parserTo){
            this.name = name;
            this.map = map;
            this.extension = extension;
            this.parserTo = parserTo;
        }

        public static Entry of(String name, String extension, SaveMap map, ParserToString parserTo) {
            return new Entry(name, extension, map, parserTo);
        }

        public String getName() {
            return name;
        }

        public String getExtension() {
            return extension;
        }

        public SaveMap getMap() {
            return map;
        }

        public byte[] getBytes() {
            return parserTo.apply(map).getBytes(CURRENT_VERSION.CHAR_SET);
        }
    }

    public interface ParserToString extends Function<SaveMap, String> { }
    public interface ParserFromString extends Function<InputStream, SaveMap> { }
}
