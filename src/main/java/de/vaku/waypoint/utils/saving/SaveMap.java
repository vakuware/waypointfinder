package de.vaku.waypoint.utils.saving;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SaveMap {
    private final Map<String, Object> save;

    public SaveMap() {
        save = new HashMap<>();
    }

    public SaveMap(Map<String, Object> map) {
        save = map;
    }

    public void put(String key, Object value) {
        save.put(key, value);
    }

    public boolean contains(String key) {
        return save.containsKey(key);
    }

    public Object get(String key) {
        return save.get(key);
    }

    public String getString(String key) {
        return get(key, String.class);
    }

    public int getInt(String key) {
        return get(key, Integer.class);
    }

    public float getFloat(String key) {
        return get(key, Float.class);
    }

    public double getDouble(String key) {
        return get(key, Float.class);
    }

    public List<?> getList(String key) {
        return get(key, List.class);
    }

    @SuppressWarnings("unchecked")
    public List<String> getStringList(String key) {
        List<?> result = getList(key);
        try {
            return (List<String>) result;
        } catch (Exception ignored) {
            throw new IllegalStateException("Given List is not of type String");
        }
    }

    public <T> T get(String key, Class<T> clazz) {
        Object value = save.get(key);
        if (clazz.isInstance(value)) {
            return clazz.cast(value);
        } else {
            throw new IllegalStateException(value + " is not of type " + clazz.getSimpleName());
        }
    }

    public Map<String, Object> getMap() {
        return save;
    }

}
