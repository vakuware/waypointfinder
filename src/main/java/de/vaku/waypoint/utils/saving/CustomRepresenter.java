package de.vaku.waypoint.utils.saving;

import de.vaku.waypoint.utils.table.Table;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;
import org.yaml.snakeyaml.representer.Representer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class CustomRepresenter extends Representer {
    public CustomRepresenter() {
        this.representers.put(UUID.class, new RepresentUUID());
        this.representers.put(Table.class, new RepresentTable());
    }

    private class RepresentUUID implements Represent {
        @Override
        public Node representData(Object o) {
            UUID uuid = (UUID) o;
            return representScalar(new Tag("!uuid"), uuid.getMostSignificantBits() + " " + uuid.getLeastSignificantBits());
        }
    }

    private class RepresentTable implements Represent {
        @Override
        public Node representData(Object o) {
            Table table = (Table) o;
            Map<String, Object> map = new HashMap<>();
            map.put("name", table.getName());
            map.put("columns", table.columns().stream().map(Table.Column::getHeader).collect(Collectors.toList()));
            Map<String, Object> data = new HashMap<>();
            for (Table.Column<?> column : table.columns()) {
                data.put(column.getHeader(), table.get(column.getHeader()));
            }
            map.put("data", data);
            return representMapping(new Tag("!table"), map, DumperOptions.FlowStyle.AUTO);
        }
    }
}
