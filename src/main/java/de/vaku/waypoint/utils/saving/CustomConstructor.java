package de.vaku.waypoint.utils.saving;

import de.vaku.waypoint.utils.table.Table;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CustomConstructor extends Constructor {

    public CustomConstructor() {
        this.yamlConstructors.put(new Tag("!uuid"), new ConstructUUID());
        this.yamlConstructors.put(new Tag("!table"), new ConstructTable());
    }

    private class ConstructUUID extends AbstractConstruct {
        @Override
        public Object construct(Node node) {
            Object o = constructScalar((ScalarNode) node);
            String[] uuidBits = o.toString().split(" ");
            return new UUID(Long.parseLong(uuidBits[0]), Long.parseLong(uuidBits[1]));
        }
    }

    @SuppressWarnings("unchecked")
    private class ConstructTable extends AbstractConstruct {
        @Override
        public Object construct(Node node) {
            Map<Object, Object> map = constructMapping((MappingNode) node);
            String name = (String) map.get("name");
            List<String> columns = (List<String>) map.get("columns");
            Map<String, List<?>> data = (Map<String, List<?>>) map.get("data");
            Table table = new Table(name);
            for (String column : columns) {
                table.insert(column, data.get(column));
            }
            return table;
        }
    }
}
