package de.vaku.waypoint.utils.saving;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class SaveVersion {
    public static final SaveVersion V_1_0 = new SaveVersion(1, 0, StandardCharsets.UTF_8);

    public final int VERSION;
    public final int SUB_VERSION;
    public Charset CHAR_SET;

    public SaveVersion(int version, int subVersion, Charset charset) {
        VERSION = version;
        SUB_VERSION = subVersion;
        CHAR_SET = charset;
    }


}
