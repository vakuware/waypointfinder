package de.vaku.waypoint.utils.markdown;

import de.vaku.waypoint.Main;
import de.vaku.waypoint.utils.Utils;
import javafx.scene.text.Text;
import org.commonmark.node.*;
import org.commonmark.renderer.NodeRenderer;

import java.util.*;
import java.util.stream.Collectors;

public class FXNodeRenderer extends AbstractVisitor implements NodeRenderer {
    public static final String BULLET_POINT = "\u2022";
    public static final String TAB = "  ";
    public static final int[] HEADING_SIZE = {
            20, 18, 16, 14, 12, 11
    };
    private final List<Text> texts = new ArrayList<>();
    private final Map<Node, Object> nodes = new HashMap<>();

    @Override
    public void visit(Heading heading) {
        //Main.LOGGER.info("heading " + heading);
        nodes.put(heading, "-fx-font-size: " + HEADING_SIZE[heading.getLevel() - 1] + "pt;");
        visitChildren(heading);
        nodes.remove(heading);
        texts.add(new Text("\n"));
    }

    @Override
    public void visit(HardLineBreak hardLineBreak) {
        //Main.LOGGER.info("hard break" + hardLineBreak);
        visitChildren(hardLineBreak);
        texts.add(new Text("\n"));
    }

    @Override
    public void visit(ThematicBreak thematicBreak) {
        //Main.LOGGER.info("thematic" + thematicBreak);
        super.visit(thematicBreak);
        texts.add(new Text("\n"));

    }

    @Override
    public void visit(IndentedCodeBlock indentedCodeBlock) {
        //Main.LOGGER.info("intended code " + indentedCodeBlock);
        super.visit(indentedCodeBlock);
    }

    @Override
    public void visit(SoftLineBreak softLineBreak) {
        //Main.LOGGER.info("soft break " + softLineBreak);
        super.visit(softLineBreak);
        texts.add(new Text(" "));
    }

    @Override
    public void visit(Paragraph paragraph) {
        //Main.LOGGER.info("paragraph " + paragraph);
        super.visit(paragraph);
        texts.add(new Text("\n"));
        //Main.LOGGER.info("paragraph end " + paragraph);
    }

    //Lists

    @Override
    public void visit(BulletList bulletList) {
        //Main.LOGGER.info("bullet list " + bulletList);
        nodes.put(bulletList, 1);
        super.visit(bulletList);
        nodes.remove(bulletList);
        //Main.LOGGER.info("bullet list end");
    }

    @Override
    public void visit(OrderedList orderedList) {
        //Main.LOGGER.info("ordered list " + orderedList);
        nodes.put(orderedList, 1);
        super.visit(orderedList);
        nodes.remove(orderedList);
    }

    @Override
    public void visit(ListItem listItem) {
        Block parent = listItem.getParent();
        String pre = Utils.repeat(TAB, (int) getCount(ListBlock.class) - 1);
        if (parent instanceof BulletList) {
            pre += BULLET_POINT;
        } else if (parent instanceof OrderedList) {
            pre += nodes.get(parent) + ".";
        }
        nodes.put(parent, (int)nodes.get(parent) + 1);
        pre += " ";
        texts.add(new Text(pre));
        super.visit(listItem);
    }

    //Emphasis

    @Override
    public void visit(StrongEmphasis strongEmphasis) {
        //Main.LOGGER.info("strong emphasis " + strongEmphasis);
        nodes.put(strongEmphasis, "-fx-font-weight: bold;");
        super.visit(strongEmphasis);
        nodes.remove(strongEmphasis);
    }

    @Override
    public void visit(Emphasis emphasis) {
        //Main.LOGGER.info("emphasis " + emphasis);
        nodes.put(emphasis, "-fx-font-style: italic;");
        super.visit(emphasis);
        nodes.remove(emphasis);
    }

    @Override
    public void visit(BlockQuote blockQuote) {
        //Main.LOGGER.info("quote " + blockQuote);
        super.visit(blockQuote);
    }

    @Override
    public void visit(HtmlInline htmlInline) {
        //Main.LOGGER.info("" + htmlInline);
        super.visit(htmlInline);
        if (htmlInline.getLiteral().equals("<br>")) {
            texts.add(new Text("\n"));
        }
    }

    @Override
    public void visit(HtmlBlock htmlBlock) {
        Main.LOGGER.info("" + htmlBlock);
        super.visit(htmlBlock);
    }

    @Override
    public void visit(Document document) {
        //Main.LOGGER.info("document " + document);
        super.visit(document);
        texts.add(new Text("\n"));
    }

    @Override
    public void visit(org.commonmark.node.Text text) {
        Text fxText = new Text(text.getLiteral());
        fxText.setStyle(nodes.values().stream()
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.joining(" ")));
        texts.add(fxText);
        visitChildren(text);
    }

    public List<Text> getTexts() {
        return texts;
    }

    public long getCount(Class<?> clazz) {
        return nodes.keySet().stream().filter(clazz::isInstance).count();
    }

    @Override
    public Set<Class<? extends Node>> getNodeTypes() {
        return new HashSet<>(Arrays.asList(
                Document.class,
                Heading.class,
                Paragraph.class,
                BlockQuote.class,
                BulletList.class,
                FencedCodeBlock.class,
                HtmlBlock.class,
                ThematicBreak.class,
                IndentedCodeBlock.class,
                Link.class,
                ListItem.class,
                OrderedList.class,
                Image.class,
                Emphasis.class,
                StrongEmphasis.class,
                org.commonmark.node.Text.class,
                Code.class,
                HtmlInline.class,
                SoftLineBreak.class,
                HardLineBreak.class
        ));
    }

    @Override
    public void render(Node node) {
        node.accept(this);
    }
}
