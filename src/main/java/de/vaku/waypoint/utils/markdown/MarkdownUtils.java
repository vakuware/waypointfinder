package de.vaku.waypoint.utils.markdown;

import de.vaku.waypoint.utils.Utils;
import javafx.scene.text.Text;
import org.commonmark.parser.Parser;

import java.util.List;

public class MarkdownUtils {
    public static List<Text> parseMarkdown(String resPath) {
        String str = Utils.readResFile(resPath, "\n");

        FXNodeRenderer visitor = new FXNodeRenderer();
        Parser parser = Parser.builder().build();
        visitor.render(parser.parse(str));

        return visitor.getTexts();
    }
}
