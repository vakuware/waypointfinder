package de.vaku.waypoint;

import de.vaku.waypoint.plugins.imp.intype.InType;
import de.vaku.waypoint.outtype.OutType;
import de.vaku.waypoint.utils.table.Table;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WaypointFile {

    private final List<Waypoint> waypointList;
    private final Table waypointTable;
    private final Main main;

    private String routeName = "UNKNOWN";

    private final File file;
    private final InType<?> inType;
    private OutType outType;

    public WaypointFile(Main main, File file, InType<?> inType) {
        waypointList = new ArrayList<>();
        this.main = main;
        this.file = file;
        this.inType = inType;
        this.outType = null;
        this.waypointTable = inType.getWaypointTable(file);
    }

    public void setOutType(OutType outType) {
        this.outType = outType;
        main.appendStatusText("Selected " + outType.getExtension() + " as output file type");
    }


    public boolean hasWaypoints() {
        return waypointTable !=null;
    }

    public Table getWaypointTable() {
        return waypointTable;
    }

    public void updateWaypoints() {
        double remain = 0;
        for (int i = 0; i < waypointList.size(); i++) {
            Waypoint wp = waypointList.get(i);
            if (i > 0)
                wp.calcCourse(waypointList.get(i - 1));
            if (wp.length.hasValue())
                remain += wp.length.getValue();
        }
        for (Waypoint wp : waypointList) {
            if (wp.length.hasValue())
                remain -= wp.length.getValue();
            wp.remain.setValue(remain);
        }
    }

    public void writeToFile() {
        if (file == null) {
            main.appendStatusText("You have to choose a source file first");
            return;
        }
        boolean success = true;
        Table table = inType.getWaypointTable(file);
        if (success) {
            String outFileName = getRouteName() + "_convert." + outType.getExtension();
            if(outType.writeToFile(this, outFileName)) {
                main.appendStatusText("Successfully written to " + outFileName);
                main.appendStatusText("Stay save - Have a good watch :)");
                main.appendStatusText("Salvo errore calculi");
            } else {
                main.appendStatusText("Could not write to " + outFileName);
            }
        }
    }

    public String getRouteName() {
        return routeName;
    }

    public File getFile() {
        return file;
    }

    public Main getMain() {
        return main;
    }

    public List<Waypoint> getWaypointList() {
        return waypointList;
    }

    public void addWaypoint(Waypoint waypoint) {
        waypointList.add(waypoint);
    }

    public void clearWaypoints() {
        waypointList.clear();
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

}
